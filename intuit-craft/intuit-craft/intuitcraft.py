#!/usr/bin/python
from __future__ import absolute_import
__author__ = "Chenna Vemula"
__email__ = " mindmap@example.com"
__version__ = " 0.0.1"
import cx_Oracle
import json
import os
import pprint
import boto3
import subprocess
import botocore.session
from flask import Flask, request, render_template
from string import Template
from datetime import timedelta, date, datetime, timezone
from botocore.client import Config
from dateutil import tz

application = Flask(__name__, template_folder='./templates')
user = 'pythonrds'
password = 'amazon$123'

#  EC2_AVAIL_ZONE=`curl -s http://169.254.169.254/latest/meta-data/placement/availability-zone`
#  EC2_REGION=`echo $EC2_AVAIL_ZONE | sed 's/[a-z]$// '`

@application.route("/")
def hello():
    data_dict={}
    srctime = datetime.now()
    totimezone = 'US/Central'
    curtime = srctime.strftime("%m-%d-%Y %I:%M:%S %p")
    from_zone = tz.tzutc()
    to_zone = tz.gettz(totimezone)
    utc = datetime.strptime(curtime, '%m-%d-%Y %I:%M:%S %p')
    utc = utc.replace(tzinfo=from_zone)
    now = utc.astimezone(to_zone).strftime("%m-%d-%Y %I:%M:%S %p")
    data_dict['Date'] = now+'-CST'
    s3 = boto3.client('s3', region_name = 'us-east-2', config=Config(signature_version='s3v4'))
    url = s3.generate_presigned_url('get_object', Params = {'Bucket': 'python-craft-app', 'Key': 'python-logo.png'}, ExpiresIn = 100)
    data_dict['Image']=url
    dns = cx_Oracle.makedsn('craft.cnh6eteiadbx.us-west-1.rds.amazonaws.com', '1521', 'CRAFTDEV')
    ora_conn = cx_Oracle.connect(user, password, dns)
    #print (ora_conn.version)
    cur = ora_conn.cursor()
    cur.execute("SELECT * FROM python EmployeeID=124")
    for row in cur:
        data_dict['DbString']=row
        print(row)
    #print(cur)
    print("LISTENINGON-SOCKET")
    return render_template('app.html', data=data_dict)

ORACLE_HTML_TEMPLATE = Template("""
                            <h1>DataBaseConnectionStatus</h1>
                            <h2>${place_name}</h2>
                            <h3>oracle_client_ver:-${ora_version}</h3>""")

@application.route("/health")
def health():
    try:
        dns = cx_Oracle.makedsn('craft.cnh6eteiadbx.us-west-1.rds.amazonaws.com', '1521', 'CRAFTDEV')
        ora_conn = cx_Oracle.connect(user, password, dns)
        ora_ver = ora_conn.version
        cur = ora_conn.cursor()
        dbstatus = 'Ok'
        ora_version = ora_ver
    except Exception as e:
        dbstatus="unable to connect to Database"
        ora_version = "Can'tRead"
    return (ORACLE_HTML_TEMPLATE.substitute(place_name=dbstatus,ora_version=ora_version))
    
@application.route("/diag")
def diag():
    return_data={}
    filters = [{'Name': 'instance-state-name', 'Values': ['running']}]
    inst_count = 0
    my_session = boto3.session.Session()
    my_region = my_session.region_name
    #region = subprocess.getstatusoutput("curl http://169.254.169.254/latest/dynamic/instance-identity/document | grep region|awk -F\" '{print $4}'")
    print(my_region)
    conn_ec2 =boto3.resource('ec2',region_name = 'us-west-1')
    conn_ec2_reg_client = boto3.client('ec2',region_name = 'us-west-1',config=Config(retries={'max_attempts': 20}))
    if len([i for i in conn_ec2.instances.filter(Filters=filters)])==0:
        inst_count=0
        print("-------->>Founded 0 Instances for this region")
    else:
        done= False
        NextToken = None
        while not done:
            if NextToken:
                instances = [i for i in conn_ec2.instances.filter(Filters=filters,NextToken=NextToken)]
            else:
                instances = [i for i in conn_ec2.instances.filter(Filters=filters)]
            for instance in instances:
                return_data[instance.id]={}
                inst_count +=1
                try:
                    response = conn_ec2_reg_client.describe_instance_status( InstanceIds=[instance.id,],)
                    instance_status = response['InstanceStatuses'][0]['InstanceStatus']['Status']
                    return_data[instance.id] = instance_status
                except:
                    return_data[instance.id] = 'UnKnown'
            if 'NextToken' in instances:
                NextToken=instances['NextToken']
            else:
                done = True
    nginx_ver = subprocess.getstatusoutput("nginx -v")
    return render_template('diag.html', return_data=return_data,  inst_count=inst_count, nginx_ver=nginx_ver)

if __name__ == '__main__':
      application.run()
# @app.errorhandler(InvalidUsage)
# def handle_invalid_usage(error):
#     response = jsonify(error.to_dict())
#     response.status_code = error.status_code
#     return response


# @app.route('/foo')
# def get_foo():
#     raise InvalidUsage('This view is gone', status_code=410)