from setuptools import setup, find_packages

setup(
    name='python-craft',
    version='0.0.1',
    packages=['python-craft'],
    install_requires=['flask','boto3','requests','uwsgi','cx_Oracle','subprocess32'],
    #dependency_links=['http://example.com/yourfiles'],
    #packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    author="Chenna Vemula",
    author_email="mindmap@example.com",
    description="AWS health python API Python3 Distribution untilities",
    classifiers=[
        "Development Status :: 1 - Development",
        "Programming Language :: Python :: 3.6",
    ]
)