# python-CRAFT FLASK PYTHON APPLICATION

## Installation

Ensure that you have setuptools installed and run

```
# python3 setup.py install

```

## Run

```
uwsgi --http-socket  127.0.0.1:3031  --wsgi-file wsgi.py --master --processes 1 --threads 1  --stats 127.0.0.1:9191
```


Project:

This repository is a code base for a FLASK HTTP API called python-CRAFT.

The python-CRAFT API is implemented in Python 3.6, the code is in the file pythoncraft.py.The type of python web framework used here is flask and is front ended by Nginx as reverse proxy and uwsgi a python web server gateway interface used to run multiple instances of the web app.

The supported APIs are

APIs
/
/diag
/health

Return codes:
200 will return a successful value /,/diag,/health accordingly


## Deployment
Check the cloudformation directory for the deployment templates in to AWS with high availability 

Testing:
Yet to be implemented 

## Details

A three-tier application App tier is implemented in Python:

###  Temp URI :   https://devopstemple.com 

The default route / prints 

“Hello World! <current time-stamp>”  an image from S3 bucket and a string fetched from the database tier 

“/health” route prints 

checks connectivity to RDS and  returns “OK” if the connectivity exists

Another route “/diag” should print

Number of instances in the cluster in its region
Version of Nginx deployed
Health of each instance 
 

