##Docker file for multiple environments
##FROM nextcode/uwsgi-nginx:latest
############################################
###########  DEVELOPMENT PURPOSE ONLY#######
############################################

FROM  amazonlinux:2

RUN yum install -y zip unzip nginx awscli python36-virtualenv  python36-pip mod24_wsgi-python36 python36-devel gcc make automake  gcc-c++ openssl-devel mod24_wsgi-python36

EXPOSE 80  

RUN ls

COPY AWS/cloudformation/files/instantclient-basic-linux.x64-18.3.0.0.0dbru.zip /tmp/

RUN mkdir -p /opt/oracle && \
    unzip /tmp/instantclient-basic-linux.x64-18.3.0.0.0dbru.zip /opt/oracle/ && \
    echo /opt/oracle/instantclient_18_3 > /etc/ld.so.conf.d/oracle-instantclient.conf && \ 
    ldconfig && \
    export LD_LIBRARY_PATH=/opt/oracle/instantclient_18_3:$LD_LIBRARY_PATH 

COPY AWS/cloudformation/files/nginx.conf  /etc/nginx/nginx.conf

RUN groupadd www-data \
    adduser www-data -g www-data \
    adduser nginx -g www-data \
    mkdir /var/www-data /var/www-data 

COPY python-craft/ . 

RUN python3 setup.py install  

COPY build/lib/python-craft /var/www-data/python-craft/

RUN chown -R www-data:www-data /var/www-data

WORKDIR /var/www-data/python-craft 

USER www-data

CMD service nginx start && uwsgi --socket  /var/www-data/python-craft/uwsgi.sock --wsgi-file uwsgi.py --master --processes 1 --threads 1 --chmod-socket 776 
