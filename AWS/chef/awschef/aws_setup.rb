require 'chef/provisioning/aws_driver'
with_driver 'aws'
machine 'appserver' do
  with_machine_options bootstrap_options: {
    image_id: "ami-076e276d85f524150",
    instance_type: "t2.micro",
    key_name: "chef-client-craft"
  }
end