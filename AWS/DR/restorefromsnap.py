import boto3
import botocore
import datetime
import re
import os
import logging
region='us-west-2'
db_instance_class='db.t2.micro'
print('Loading function')
def byTimestamp(snap):
    if 'SnapshotCreateTime' in snap:
    return datetime.datetime.isoformat(snap['SnapshotCreateTime'])
    else:
    return datetime.datetime.isoformat(datetime.datetime.now())
def lambda_handler(event, context):
    rds_client = boto3.client('rds', region_name=region)
    instance= os.environ['instance']
    db_SubnetGroup = os.environ['OrclDBSubnetGroup']
    OrclRDSParamGroup = os.environ['OrclRDSParamGroup']
    DBName =os.environ['DBName']
    instances = [instance]
    for instance in instances:
        try:
            rds_client_snaps = rds_client.describe_db_snapshots(DBInstanceIdentifier = instance)['DBSnapshots']
            print "DB_Snapshots:", rds_client_snaps
            rds_client_snap = sorted(rds_client_snaps, key=byTimestamp, reverse=True)[0]['DBSnapshotIdentifier']
            snap_id = (re.sub( '-\d\d-\d\d-\d\d\d\d ?', '', rds_client_snap))
            print('Will restore %s to %s' % (rds_client_snap, snap_id))
            response = rds_client.restore_db_instance_from_db_snapshot(DBInstanceIdentifier=snap_id,DBSnapshotIdentifier=rds_client_snap,DBInstanceClass=db_instance_class,DBSubnetGroupName=db_SubnetGroup,MultiAZ=False,PubliclyAccessible=False,DBName=DBName,Port=123,DBParameterGroupName=OrclRDSParamGroup)
            print(response)
        except botocore.exceptions.ClientError as e:
            raise Exception("Could not restore: %s" % e)